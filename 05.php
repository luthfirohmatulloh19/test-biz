<?php

function recursiveNum($arr, $index, $n, $decrement) 
{ 
    // Base condition 
    if ($decrement < 0) 
        return; 
  
    // combination is found, print it 
    if ($decrement == 0) { 
        echo implode(",", $arr). "<br/>"; 
        return; 
    } 
    
    // set previos value
    $prev = ($index == 0) ? 1 : $arr[$index - 1]; 
  
    // look for list numbers if added equals n
    for ($k = $n; $k >= $prev ; $k--) { 
        $arr[$index] = $k; 
        recursiveNum($arr, $index + 1, $n, $decrement - $k); 
    } 
} 
  
function getCombination($n){
    $arr = array(); 
    recursiveNum($arr, 0, $n, $n); 
} 
  
$n = 4; 
getCombination($n); 

?>
 