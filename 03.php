<?php

// PRIME NUMBERS
function isPrimes($number){
	for($i = 2; $i < $number; $i++)
		if($number % $i == 0)
			return false;
	return true;
}

function getPrimes($n){
	$arr = [];
	for($i = 2; $i <= $n; $i++)
		if(isPrimes($i))
			array_push($arr, $i);
	return $arr;
}


echo implode(",", getPrimes(5));

?>